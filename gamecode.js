var _gameScenes =
{
	TITLE: 0,
	GAMEPLAY: 1,
	ENDING: 2
}
var _levels =
{
	NULL: 0,
	LEVEL_1: 1,
	LEVEL_2: 2,
	LEVEL_3: 3
}
var _dialoguePhase =
{
	BEGINNING: 0,
	ENGING: 1
}

//----------

var currentGameScene = _gameScenes.TITLE;
var currentLevel = _levels.NULL;
var currentDialoguePhase = _dialoguePhase.BEGINNING;
var currentDialogueIndex = 0;

var $gameData;
$.getJSON("gameplay_data.json", function(data)
{
	$gameData = data;
});

$(function() 
{
	//Hides all parent elements and THEN shows the title screen (AKA fade from black)
	$("body").children().hide();
	$("#title").fadeIn(1000, function()
	{
		$(".title_content").find("button").animate({opacity: "1"}, 1000);
	});
	console.log($gameData);
})



function startGame()
{
	$("#title").fadeOut(1000, function()
	{
		currentLevel = _levels.LEVEL_1;
		console.log(currentLevel);
		$(".gameplay_content_dialogue").hide();
		$("#choices").hide();
		$("#next").hide();
		$("#gameplay").fadeIn(1000, function()
		{
			$(".gameplay_content_dialogue").slideDown(500, function()
			{
				$(".gameplay_content_dialogue").find("p").hide();
				loadDialogue();
			});
		});
		console.log("Game started");
	});
	
}

function loadDialogue()
{
	
	$(".gameplay_content_dialogue").find("p").hide();
	$("#choices").hide();
	$("#next").hide();

	var levelString = "level" + currentLevel;
	var currentDialogueText = $gameData[levelString].dialogue1[currentDialogueIndex];
	$(".gameplay_content_dialogue").find("p").html(currentDialogueText);
	$(".gameplay_content_dialogue").find("p").slideDown(500, function()
	{
		currentDialogueIndex++;

		var dialogueArray = $gameData[levelString].dialogue1;
		console.log(dialogueArray.length + " , " + currentDialogueIndex);
		
		if(currentDialogueIndex < dialogueArray.length)
		{
			$("#next").fadeIn(500);
		}
		else
		{
			currentDialogueIndex = 0;
			$("#choices").fadeIn(500);
		}
	});
}